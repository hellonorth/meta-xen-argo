# Implement the argo DISTRO_FEATURE for the xen hypervisor

FILESEXTRAPATHS:prepend := "${THISDIR}/xen:"

SRC_URI:append = " \
    ${@bb.utils.contains('DISTRO_FEATURES', 'argo', 'file://argo.cfg', '', d)} \
    "

check_argo_is_enabled_config() {
    if [ -n "${@bb.utils.contains('DISTRO_FEATURES', 'argo', 'A', '', d)}" ] &&\
        ! grep -q '^CONFIG_ARGO=y$' ${S}/xen/.config ; then
        bbfatal 'Failed to enable Argo'
    fi
}

do_compile:prepend() {
    check_argo_is_enabled_config
}

do_compile:append() {
    if [ -n "${@bb.utils.contains('DISTRO_FEATURES', 'argo', 'A', '', d)}" ] &&\
        ! grep -q ' do_argo_op$' ${B}/xen/xen-syms.map ; then
        bbfatal 'Failed to find Argo hypercall op symbol'
    fi
}

do_install:prepend() {
    check_argo_is_enabled_config
}

do_deploy:prepend() {
    check_argo_is_enabled_config
}
